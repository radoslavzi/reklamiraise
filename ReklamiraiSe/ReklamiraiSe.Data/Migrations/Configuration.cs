namespace ReklamiraiSe.Data.Migrations
{
	using System.Data.Entity.Migrations;
	using Microsoft.AspNet.Identity;
	using Microsoft.AspNet.Identity.EntityFramework;
	using ReklamiraiSe.Helper.Permissions;
	using ReklamiraiSe.Models;
	using ReklamiraiSe.Shared;
	using System;
	using System.Data.Entity.Validation;

	public sealed class Configuration : DbMigrationsConfiguration<ReklamiraiSe.Data.ReklamiraiSeDbContext>
	{
		RoleManagement _roleManagement;

		public Configuration()
		{
			this.AutomaticMigrationsEnabled = true;
			this.AutomaticMigrationDataLossAllowed = true;
			this._roleManagement = new RoleManagement();
		}

		protected override void Seed(ReklamiraiSe.Data.ReklamiraiSeDbContext context)
		{
			#region createadmin
			this.AddSpecialAdmin(context);
			#endregion

			#region create regular user role
			this._roleManagement.CreateRoleIfNotExists(context, ReklamiraiSe.Shared.Constants.RegularUsersRole);
			#endregion
		}

		public void AddSpecialAdmin(ReklamiraiSeDbContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("Context cannot be null!");
			}

			UserManager<User> userManager = new UserManager<User>(new UserStore<User>(context));
			User admin = Generator.BuildSampleUser();
			string roleName = this._roleManagement.CreateRoleIfNotExists(context, Shared.Constants.SpecialAdminRole);

			var userExists = userManager.FindByName(admin.UserName) != null;

			if (!userExists)
			{
				userManager.Create(admin, admin.Password);
				userManager.AddToRole(admin.Id, roleName);
			}
		}
	}
}