﻿using ReklamiraiSe.Data.Configuration.Interface;
using ReklamiraiSe.Helper.ErrorLogs;
using ReklamiraiSe.Models;
using System;
using System.Threading;


namespace ReklamiraiSe.Data.Configuration
{
    public static class EntitiesRepository
    {

        static EntitiesRepository()
        {
            Messages = new AsyncEntityDirector<Message, int>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.MessagesCache);
            Feedbacks = new AsyncEntityDirector<Feedback, int>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.FeedbacksCache);
            Comments = new AsyncEntityDirector<Comment, int>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.CommentsCache);
            Images = new AsyncEntityDirector<ImageInfo, int>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.ImageInfoCache);
            Products = new AsyncEntityDirector<Product, int>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.ProductsCache);
            Users = new AsyncEntityDirector<User, string>(ReklamiraiSeDbContext.Create(), EntitiesCacheRepository.UsersCache);
        }

        public static IAsyncEntityOperations<Product, int> Products { get; private set; }
        public static IAsyncEntityOperations<ImageInfo, int> Images { get; private set; }
        public static IAsyncEntityOperations<Comment, int> Comments { get; private set; }
        public static IAsyncEntityOperations<Feedback, int> Feedbacks { get; private set; }
        public static IAsyncEntityOperations<Message, int> Messages { get; private set; }
        public static IAsyncEntityOperations<User, string> Users { get; private set; }
    }
}
