﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReklamiraiSe.Data.Configuration.Interface
{
    public interface IEntityOperations<EntityType, Object>
    {
        IQueryable<EntityType> All();

        EntityType GetById(Object id);

        void Add(EntityType data);

		void Update(Object id, EntityType data);

        void Delete(Object id);
	}
}
