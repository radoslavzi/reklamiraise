﻿namespace ReklamiraiSe.Data.Configuration.Interface
{
    public interface IAsyncEntityOperations<EntityType, Object> : IEntityOperations<EntityType, Object>
    {
        void DeleteAsync(Object id);
    }
}
