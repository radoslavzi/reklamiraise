﻿namespace ReklamiraiSe.Data.Configuration.Interface
{
    public interface IEntitiesCacheLoader
    {
        void LoadDataToCacheAsync();
    }
}
