﻿using ReklamiraiSe.Models;
using ReklamiraiSe.Data.Configuration.Interface;
using System.Threading;
using ReklamiraiSe.Data.CacheSystem.Interface;

namespace ReklamiraiSe.Data.Configuration
{
    public class AsyncEntityDirector<EntityType, Object> : EntityDirector<EntityType, Object>, IAsyncEntityOperations<EntityType, Object>
        where EntityType : class, IModelable<Object>
    {
        private ReklamiraiSeDbContext _context;
        private readonly object threadLock = new object();

        public AsyncEntityDirector(ReklamiraiSeDbContext context, ICacheProvider<EntityType, Object> cache)
            : base(context, cache)
        {
            this._context = context;
            this._cache = cache;
        }

        public async void DeleteAsync(Object id)
        {
            Monitor.Enter(threadLock);
            EntityType data = this.GetById(id);
            this.entity.Remove(data);
            await this._context.SaveChangesAsync();
            this._cache.Delete(id);
            Monitor.Exit(threadLock);
        }
    }
}