﻿using ReklamiraiSe.Data.Configuration.Interface;
using System.Threading.Tasks;

namespace ReklamiraiSe.Data.Configuration
{
    public class EntitiesCacheLoader : IEntitiesCacheLoader
    {
        public static Task ProductsThread;
        public static Task UsersThread;


        public EntitiesCacheLoader()
        {
            ProductsThread = new Task(() => LoadProductsCache());
            UsersThread = new Task(() => LoadUsersCache());
        }

        public void LoadDataToCacheAsync()
        {
            ProductsThread.Start();
        }

        private void LoadProductsCache()
        {
            var products = EntitiesRepository.Products.All();
            EntitiesCacheRepository.ProductsCache.AddRange(products);
        }

        private void LoadUsersCache()
        {
            var users = EntitiesRepository.Users.All();
            EntitiesCacheRepository.UsersCache.AddRange(users);
        }
    }
}