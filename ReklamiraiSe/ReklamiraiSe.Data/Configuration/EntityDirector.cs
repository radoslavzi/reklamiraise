﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ReklamiraiSe.Helper.Extensions;
using ReklamiraiSe.Models;
using ReklamiraiSe.Data.Configuration.Interface;
using ReklamiraiSe.Data.CacheSystem.Interface;
using System.Data.Entity.Validation;

namespace ReklamiraiSe.Data.Configuration
{
	public class EntityDirector<EntityType, Object> : IEntityOperations<EntityType, Object>
		where EntityType : class, IModelable<Object>
	{
		private ReklamiraiSeDbContext _context;
		protected DbSet<EntityType> entity;
        protected ICacheProvider<EntityType, Object> _cache;

        public EntityDirector(ReklamiraiSeDbContext context, ICacheProvider<EntityType, Object> cache)
		{
            if (context == null)
            {
                throw new ArgumentNullException("An instance of ReklamiraiSeDbContext is missing.");
            }

            this._context = context;
            this.entity = this._context.Set<EntityType>();

			this._cache = cache;
		}

        public IQueryable<EntityType> All()
		{
            return this.entity.AsQueryable(); ;
		}

		public EntityType GetById(Object id)
		{
			if (this._cache.Get(id) != null)
			{
                return this._cache.Get(id);
			}
			else
			{
				EntityType entityRow = this.entity.Find(id);
				return entityRow;
			}
		}

		public void Add(EntityType data)
		{
            try {
			if (data == null)
			{
				throw new ArgumentNullException("Data cannot be null.");
			}

			this.entity.Add(data);
            try {
			this._context.SaveChanges();
            }
            catch (DbEntityValidationException exx)
            {
                foreach (var item in exx.EntityValidationErrors)
                {

                }
            }
			this._cache.Add(data);
		}
            catch(DbEntityValidationException ex)
            {
                var errors = ex.EntityValidationErrors;

            }
		}

		private void UpdateData(EntityType entityRow, EntityType data)
		{
			this._context.Entry(entityRow).CurrentValues.SetValues(data);
			this.UpdateItemInCacheIfExists(data);
		}

		private void UpdateItemInCacheIfExists(EntityType data)
		{
			if (this._cache.Get(data.Id) != null)
			{
				this._cache.Update(data.Id, data);
			}
		}

		public void Update(Object id, EntityType data)
		{
			var entityRow = this.GetById(id);
			UpdateData(entityRow, data);
			this._context.SaveChanges();
			this.UpdateItemInCacheIfExists(data);
		}

		public void Delete(Object id)
		{
			EntityType data = this.GetById(id);

			this.entity.Remove(data);
			this._context.SaveChanges();
            this._cache.Delete(id);
		}
	}
}