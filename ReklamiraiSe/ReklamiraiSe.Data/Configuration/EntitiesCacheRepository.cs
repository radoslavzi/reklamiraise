﻿using ReklamiraiSe.Models;
using ReklamiraiSe.Data.CacheSystem;
using ReklamiraiSe.Data.CacheSystem.Interface;

namespace ReklamiraiSe.Data.Configuration
{
    public class EntitiesCacheRepository
    {
        static EntitiesCacheRepository()
        {
            CommentsCache = new CommentsCache();
            FeedbacksCache = new FeedbacksCache();
            MessagesCache = new MessagesCache();
            ProductsCache = new ProductsCache();
            ImageInfoCache = new ImageInfoCache();
            UsersCache = new UsersCache();
        }

        public static ICacheProvider<Product, int> ProductsCache;
        public static ICacheProvider<Comment, int> CommentsCache;
        public static ICacheProvider<Feedback, int> FeedbacksCache;
        public static ICacheProvider<Message, int> MessagesCache;
        public static ICacheProvider<ImageInfo, int> ImageInfoCache;
        public static ICacheProvider<User, string> UsersCache;
    }
}