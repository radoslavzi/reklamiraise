﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using ReklamiraiSe.Models;
using System.Data.Entity.Infrastructure;
/// <summary>
/// <explanation>DBContext brand ReklamiraiSe</explanation>
/// <author>Radoslav Ivanov</author>
/// </summary>
namespace ReklamiraiSe.Data
{
    public class ReklamiraiSeDbContext : IdentityDbContext<User>, IDisposable
    {
        public ReklamiraiSeDbContext()
            : base("ReklamiraiSeConnection")
        {
            Database.SetInitializer<ReklamiraiSeDbContext>(null);
        }

        public IDbSet<Product> Products { get; set; }

        public IDbSet<Comment> Comments { get; set; }

        public IDbSet<Feedback> Feedbacks { get; set; }

        public IDbSet<Message> Messages { get; set; }

		public IDbSet<ImageInfo> ImageInfos { get; set; }

        public IDbSet<Category> Categories { get; set; }

        public static ReklamiraiSeDbContext Create()
        {
            return new ReklamiraiSeDbContext(); ;
        }

        void IDisposable.Dispose()
        {
            base.Dispose();
        }
    }
}
