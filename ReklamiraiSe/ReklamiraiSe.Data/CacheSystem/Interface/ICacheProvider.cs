﻿using ReklamiraiSe.Models;
using System.Collections.Generic;
using System.Linq;

namespace ReklamiraiSe.Data.CacheSystem.Interface
{
    public interface ICacheProvider<EntityType, Object>
        where EntityType : class
    {
        void Add(EntityType model);
        IQueryable<EntityType> All();
        void Delete(Object id);
        EntityType Get(Object id);
        void Update(Object id, EntityType updatedModel);
        void AddRange(IEnumerable<EntityType> models);
        bool IsEmpty();
    }
}
