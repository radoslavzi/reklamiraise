﻿using ReklamiraiSe.Data.CacheSystem.Interface;
using ReklamiraiSe.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReklamiraiSe.Data.CacheSystem
{
    public class CacheBase<EntityType, Object> : ICacheProvider<EntityType, Object>
        where EntityType : class, IModelable<Object>
    {
        private IDictionary<Object, EntityType> _cache;

        public CacheBase()
        {
            this._cache = new Dictionary<Object, EntityType>();
        }

        public void Add(EntityType entity)
        {
            this._cache.Add(entity.Id, entity);
        }

        public void Delete(Object id)
        {
            if (this._cache.ContainsKey(id))
            {
                this._cache.Remove(id);
            }
        }

        public void Update(Object id, EntityType updatedModel)
        {
            if (this._cache.ContainsKey(id))
            {
                this._cache[id] = updatedModel;
            }
        }

        public EntityType Get(Object id)
        {
            if (!this._cache.ContainsKey(id))
            {
                return null;    
            }

            return this._cache[id];
        }

        public void AddRange(IEnumerable<EntityType> models)
        {
            foreach (var model in models)
            {
                this.Add(model);
            }
        }

        public IQueryable<EntityType> All()
        {
           return this._cache.Values.AsQueryable();
        }

        public bool IsEmpty()
        {
            return this._cache.Count == 0;
        }
    }
}
