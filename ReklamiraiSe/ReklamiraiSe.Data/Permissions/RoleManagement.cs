﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ReklamiraiSe.Data;
using System;

namespace ReklamiraiSe.Helper.Permissions
{
    public class RoleManagement
    {
        public string CreateRoleIfNotExists(ReklamiraiSeDbContext context, string roleName)
        {
            if (string.IsNullOrEmpty(roleName) || context == null)
            {
                throw new ArgumentNullException("Argument cannot be null!");
            }

            RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!RoleManager.RoleExists(roleName))
            {
                RoleManager.Create(new IdentityRole(roleName));
            }

            return roleName;
        }
    }
}
