﻿
using System.Net.Mail;
namespace ReklamiraiSeAPI.MailAPI
{
    public interface IMailApi
    {
        MailMessage ConstructMessage(string mailUsernameTo);
        void SendMail(MailMessage mail);
    }
}
