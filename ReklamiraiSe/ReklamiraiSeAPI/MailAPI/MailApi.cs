﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace ReklamiraiSeAPI.MailAPI
{
    public class MailApi : IMailApi
    {
        private string _mailPassword;
        private string _mailAddressFrom;
        private int _portNumber;
        private string _hostName;
        private bool _enableSSL;

        private string _subject;
        private string _body;

        public MailApi(string subject, string body)
        {
            this._mailAddressFrom = ConfigurationManager.AppSettings["mailaddress"];
            this._mailPassword = ConfigurationManager.AppSettings["mailpassword"];
            this._hostName = ConfigurationManager.AppSettings["hostname"];
            this._subject = subject;
            this._body = body;

            int.TryParse(ConfigurationManager.AppSettings["portnumber"], out this._portNumber);
            bool.TryParse(ConfigurationManager.AppSettings["enablessl"], out this._enableSSL);
        }

        public MailMessage ConstructMessage(string mailUsernameTo)
        {
            return new MailMessage(this._mailAddressFrom, mailUsernameTo, this._subject, this._body);
        }

        public void SendMail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient(this._hostName, this._portNumber);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(this._mailAddressFrom, this._mailPassword);
            client.EnableSsl = this._enableSSL;

            client.Send(mail);
        }
    }
}