using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using ReklamiraiSeAPI.MailAPI;

namespace ReklamiraiSeAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IMailApi, MailApi>();
            
            //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}