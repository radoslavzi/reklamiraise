﻿using ReklamiraiSe.Helper.ErrorLogs;
using ReklamiraiSeAPI.MailAPI;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;

namespace ReklamiraiSeAPI.Controllers
{
    public class MailManagerController : ApiController
    {
        private IMailApi _mailApi;

        // POST: api/MailManager
        [HttpPost]
        [Authorize]
        public HttpResponseMessage SendMail([FromBody] string mailAddress, string subject, string body)
        {
            try
            {
                this._mailApi = new MailApi(subject, body);
                MailMessage message = this._mailApi.ConstructMessage(mailAddress);
                this._mailApi.SendMail(message);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                ErrorLogger log = new ErrorLogger();
                log.LogError(ex);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
