﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
    public class Product : IModelable<int>
    {
        private ICollection<Comment> _comments;
        private ICollection<ImageInfo> _images;

        public Product()
        {
            this._comments = new HashSet<Comment>();
            this._images = new HashSet<ImageInfo>();

            this.CreationDate = DateTime.UtcNow;
            this.LastModified = DateTime.UtcNow;
            this.IsHighRank = false;
            this.Rank = 0;
        }

        [Key]
        public int Id { get; set; }

        [MinLength(5, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_title_validation_error")]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_title_required_error")]
        public string Title { get; set; }

        [MinLength(100, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_description_validation_error")]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_description_required_error")]
        public string Description { get; set; }

        public string Tags { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastModified { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_price_validation")]
        public decimal Price { get; set; }

        public int Rank { get; set; }

        public bool IsHighRank { get; set; }

        public virtual ICollection<Comment> Comments
        {
            get { return this._comments; }
            set { this._comments = value; }
        }

        public virtual ICollection<ImageInfo> Images
        {
            get { return this._images; }
            set { this._images = value; }
        }
    }
}