﻿using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
	public class ImageInfo : IModelable<int>
    {
		[Key]
		public int Id { get; set; }

		[Required]
		public string Path { get; set; }

        public string Alt { get; set; }

		[Required]
		public string Title { get; set; }
	}
}
