﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ReklamiraiSe.Models
{
    public class User : IdentityUser, IModelable<string>
    {
        private ICollection<Product> _products;

        private ICollection<User> _friends;
        private ICollection<User> _followedUsers;

        private ICollection<Message> _receivedMessages;
        private ICollection<Message> _sendMessages;

        private ICollection<Feedback> _feedbacks;

        private int _activeUserMaxNonLoginDays;
        private bool _isActive;

        public User()
        {
            this._products = new HashSet<Product>();

            this._friends = new HashSet<User>();
            this._followedUsers = new HashSet<User>();

            this._receivedMessages = new HashSet<Message>();
            this._sendMessages = new HashSet<Message>();

            this._feedbacks = new HashSet<Feedback>();

            this.IsInBlackList = false;
            this.Credits = 0;
            this._activeUserMaxNonLoginDays = 300;
            this.LastLogin = DateTime.UtcNow;
            this._isActive = DateTime.UtcNow.Subtract(TimeSpan.FromDays(this._activeUserMaxNonLoginDays)) < this.LastLogin;
        }

        [Display(Name = "registration_username", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_user_required_error")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_user_validation_error", MinimumLength = 6)]
        public override string UserName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_password_required_error")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_password_validation_error", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "registration_password", ResourceType = typeof(Resources.Resource))]
        [NotMapped]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "registration_confirm_password", ResourceType = typeof(Resources.Resource))]
        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_confirm_password_required_error")]
        [NotMapped]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_email_required_error", ErrorMessage = null)]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_email_validation_error", ErrorMessage = null)]
        [Display(Name = "registration_email", ResourceType = typeof(Resources.Resource))]
        public override string Email { get; set; }

        [Display(Name = "registration_name", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_name_required_error")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_name_validation_error", MinimumLength = 2)]
        public string Name { get; set; }

        [Display(Name = "registration_country", ResourceType = typeof(Resources.Resource))]
        public string Country { get; set; }

        [Display(Name = "registration_city", ResourceType = typeof(Resources.Resource))]
        public string City { get; set; }

        [Display(Name = "registration_phone", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_phone_required_error", ErrorMessage = null)]
        [Phone(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "registration_phone_validation_error", ErrorMessage = null)]
        public override string PhoneNumber { get; set; }

        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [Display(Name = "registration_about_you", ResourceType = typeof(Resources.Resource))]
        public string AboutMeDescription { get; set; }

        public int Rank
        {
            get
            {
                int rank = 0;

                foreach (var product in this.Products)
                {
                    rank += product.Rank;
                }

                return rank;
            }
        }

        public string ImagePath { get; set; }

        public int Credits { get; set; }

        public bool IsInBlackList { get; set; }

        public DateTime LastLogin { get; set; }

        public virtual ICollection<Product> Products
        {
            get { return this._products; }
            set { this._products = value; }
        }

        public virtual ICollection<Feedback> Feedbacks
        {
            get { return this._feedbacks; }
            set { this._feedbacks = value; }
        }

        public virtual ICollection<Message> ReceivedMessages
        {
            get { return this._receivedMessages; }
            set { this._receivedMessages = value; }
        }

        public virtual ICollection<Message> SendMessages
        {
            get { return this._sendMessages; }
            set { this._sendMessages = value; }
        }
    }
}