﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
    public class Category : IModelable<int>
    {
        private ICollection<Product> _products;

        public Category()
        {
            this._products = new HashSet<Product>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public User Author { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastModified { get; set; }


        public virtual ICollection<Product> Products
        {
            get { return this._products; }
            set { this._products = value; }
        }

        public bool IsActive
        {
            get
            {
                return true;
            }

            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
