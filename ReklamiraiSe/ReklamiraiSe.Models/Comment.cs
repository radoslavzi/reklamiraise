﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
    public class Comment : IModelable<int>
    {
        private ICollection<Comment> _comments;

		public Comment()
        {
            this._comments = new HashSet<Comment>();
		}

        [Key]
        public int Id { get; set; }

        [Required]
        public User Author { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastModified { get; set; }

		public virtual ICollection<Comment> Comments
        {
            get { return this._comments; }
            set { this._comments = value; }
        }
    }
}
