﻿using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
    public class Message : IdentityMessage, IModelable<int>
    {
        private int _messageValidationDays;
        private bool _isActive;
        public Message()
        {
            this._messageValidationDays = 300;
            this._isActive = DateTime.UtcNow.Subtract(TimeSpan.FromDays(this._messageValidationDays)) < this.CreationDate;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "The email address is not valid")]
        public string From { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        public bool IsActive
        {
            get
            {
                return this._isActive ? this._isActive : DateTime.UtcNow.Subtract(TimeSpan.FromDays(this._messageValidationDays)) < this.CreationDate;
            }
            set
            {
                this._isActive = value;
            }
        }
    }
}
