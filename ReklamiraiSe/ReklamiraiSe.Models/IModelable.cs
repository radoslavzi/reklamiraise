﻿namespace ReklamiraiSe.Models
{
	public interface IModelable<Object>
	{
        Object Id { get; set; }
	}
}
