﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ReklamiraiSe.Models
{
	public class Feedback : IModelable<int>
    {
		public Feedback()
		{
			this.IsPositive = true;
		}

		[Key]
		public int Id { get; set; }

		[Required]
		public bool IsPositive { get; set; }

		[Required]
		public string Title { get; set; }

		[Required]
		public DateTime CreationDate { get; set; }

		[Required]
		public string Description { get; set; }
	}
}
