﻿using System;

namespace ReklamiraiSe.Helper.ErrorLogs
{
    public interface IErrorLogger
    {
		void LogError(Exception ex);
    }
}
