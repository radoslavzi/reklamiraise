﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace ReklamiraiSe.Helper.ErrorLogs
{
    public class ErrorLogger : IErrorLogger, IDisposable
    {
        private DirectoryInfo _errorLogsFolder;
        private StreamWriter _streamWriter;
        private readonly object threadLock = new object();

        public ErrorLogger()
        {
            this._errorLogsFolder = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Environment.MachineName, Environment.UserName));
        }

        public void LogError(Exception ex)
        {
            Monitor.Enter(threadLock);
            this.WriteErrorMessage(ex.Source, ex.StackTrace, ex.Message);
            Monitor.Exit(threadLock);
        }

        private void WriteErrorMessage(string exceptionSource, string stackTrace, string message)
        {
            string fullPath = Path.Combine(this._errorLogsFolder.FullName, string.Concat(exceptionSource, ".txt"));
            using (this._streamWriter = new StreamWriter(fullPath, true, Encoding.UTF8))
            {
                this._streamWriter.WriteLine("--------------------------");
                this._streamWriter.Write("Message: " + message);
                this._streamWriter.WriteLine();
                this._streamWriter.Write("StackTrace: " + stackTrace);
                this._streamWriter.WriteLine("-----------------------------");
            }
        }

        public void Dispose()
        {
            this._streamWriter.Dispose();
        }
    }
}