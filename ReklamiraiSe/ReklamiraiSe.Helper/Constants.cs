﻿using System;
using System.Configuration;

namespace ReklamiraiSe.Shared
{
    public static class Constants
    {
        public readonly static string SpecialAdmin = ConfigurationManager.AppSettings["SpecialAdmin"] ?? "bulgaria";
        public const string SpecialAdminRole = "BulgarianAdmin";
        public readonly static string SpecialAdminPassword = ConfigurationManager.AppSettings["SpecialAdminPassword"] ?? "pass350355";
        public readonly static string SpecialAdminEmail = ConfigurationManager.AppSettings["SpecialAdminEmail"] ?? "admin@mail.bg";
        public readonly static string SpecialAdminName = ConfigurationManager.AppSettings["SpecialAdminName"] ?? "bulgaria";
        public readonly static string SpecialAdminPhone = ConfigurationManager.AppSettings["SpecialAdminPhone"] ?? "0888888888";
        public readonly static string RegularUsersRole = ConfigurationManager.AppSettings["UserRole"] ?? "RegularUsers";
        public readonly static string RegionCookieName = ConfigurationManager.AppSettings["RegionCookieName"] ?? "_rgn";
        public readonly static string BulgarianCultureCode = ConfigurationManager.AppSettings["BulgarianCultureCode"] ?? "bg-bg";
        public readonly static string DefaultCultureCode = ConfigurationManager.AppSettings["DefaultCultureCode"] ?? "en-gb";
        public readonly static string CultureRouteKey = ConfigurationManager.AppSettings["CultureRouteKey"] ?? "culture";
		public readonly static string NumberOfProductssPerPage = ConfigurationManager.AppSettings["NumberOfProductsPerPage"] ?? "15";
        public readonly static string CultureRouteName = ConfigurationManager.AppSettings["CultureRouteName"] ?? "culture";
	}
}
