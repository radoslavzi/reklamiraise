﻿using ReklamiraiSe.Models;

namespace ReklamiraiSe.Shared
{
    public class Generator
    {
        public static User BuildSampleUser()
        {
            return new User
            {
                Password =  Constants.SpecialAdminPassword,
                UserName =  Constants.SpecialAdmin,
                Email =  Constants.SpecialAdminEmail,
                Name =  Constants.SpecialAdminName,
                ConfirmPassword =  Constants.SpecialAdminPassword,
                PhoneNumber = Constants.SpecialAdminPhone
            };
        }
    }
}
