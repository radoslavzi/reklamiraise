﻿namespace ReklamiraiSe.Helper.Extensions
{
	public static class Object
	{
		public static bool IsNullable(this object objectToCheck)
		{
			if (objectToCheck == null)
			{
				return true;
			}

			return false;
		}
	}
}
