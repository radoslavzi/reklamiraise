﻿using System.Collections.Generic;

namespace ReklamiraiSe.Helper.Extensions
{
	public static class Collection
	{
		public static void SafeAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict,
											 TKey key, TValue value)
		{
			if (!dict.ContainsKey(key))
			{
				dict.Add(key, value);
			}
		}
	}
}
