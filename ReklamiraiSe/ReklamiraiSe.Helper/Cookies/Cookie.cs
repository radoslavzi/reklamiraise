﻿using System;
using System.Web;
using ReklamiraiSe.Helper.Extensions;

namespace ReklamiraiSe.Shared.Cookies
{
    public static class Cookie
    {
        public static HttpCookie Create(string cookieName, string value)
        {
            if (string.IsNullOrEmpty(cookieName))
            {
                throw new ArgumentNullException("Cannot create nullable cookie!");
            }

            HttpCookie cookie = new HttpCookie(cookieName, value);
            cookie.Expires = DateTime.MaxValue.ToUniversalTime();
            cookie.Shareable = true;
            cookie.HttpOnly = true;

			return cookie;
        }

		public static HttpCookie GetByName(string cookieName, HttpContextBase httpContext)
		{
			if (string.IsNullOrEmpty(cookieName) || httpContext.IsNullable())
			{
				throw new ArgumentNullException("Argument null is not valid");
			}

			HttpCookie cookie = httpContext.Request.Cookies[cookieName];
			if (cookie.IsNullable())
			{
				cookie = Create(Constants.RegionCookieName, Constants.DefaultCultureCode);
                httpContext.Response.SetCookie(cookie);
            }

			return cookie;
        }
	}
}
