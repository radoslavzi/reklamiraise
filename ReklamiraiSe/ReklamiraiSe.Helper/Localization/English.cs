﻿using ReklamiraiSe.Shared;

namespace ReklamiraiSe.Helper.Localization
{
	public class English : ICultureProvider
	{
		public string Code
		{
			get
			{
				return Constants.DefaultCultureCode;
			}
		}

		public bool IsDefault
		{
			get
			{
				return true;
			}
		}
	}
}
