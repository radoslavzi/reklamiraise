﻿using ReklamiraiSe.Shared;

namespace ReklamiraiSe.Helper.Localization
{
	public class Bulgarian : ICultureProvider
	{
		public string Code
		{
			get
			{
				return Constants.BulgarianCultureCode;
			}
		}

		public bool IsDefault
		{
			get
			{
				return false;
			}
		}
	}
}
