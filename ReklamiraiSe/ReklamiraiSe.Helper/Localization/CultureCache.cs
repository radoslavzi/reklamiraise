﻿using System;
using System.Collections.Generic;

namespace ReklamiraiSe.Helper.Localization
{
	public static class CultureCache
	{
		private static IDictionary<string, ICultureProvider> _languageProvider;

		static CultureCache()
		{
			_languageProvider = new Dictionary<string, ICultureProvider>(StringComparer.InvariantCultureIgnoreCase);
		}

		public static IDictionary<string, ICultureProvider> LanguageProvider
		{
			get
			{
				return _languageProvider;
			}

			set
			{
				_languageProvider = value;
			}
		}

		public static string CurrentLanguageCode { get; set; }
	}
}
