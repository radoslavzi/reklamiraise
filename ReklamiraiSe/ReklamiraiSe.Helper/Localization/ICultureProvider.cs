﻿namespace ReklamiraiSe.Helper.Localization
{
	public interface ICultureProvider
	{
		string Code { get; }

		bool IsDefault { get; }
	}
}
