﻿using ReklamiraiSe.Helper.Extensions;

namespace ReklamiraiSe.Helper.Localization
{
	public static class CulturesLoader
	{
		private static Bulgarian _bulgarianLanguage;
		private static English _englishLanguage;

		static CulturesLoader()
		{
			_englishLanguage = new English();
			_bulgarianLanguage = new Bulgarian();
		}

		public static void RegisterLanguages()
		{
			CultureCache.LanguageProvider.SafeAdd<string, ICultureProvider>(_bulgarianLanguage.Code, _bulgarianLanguage);
			CultureCache.LanguageProvider.SafeAdd<string, ICultureProvider>(_englishLanguage.Code, _englishLanguage);
		}
	}
}
