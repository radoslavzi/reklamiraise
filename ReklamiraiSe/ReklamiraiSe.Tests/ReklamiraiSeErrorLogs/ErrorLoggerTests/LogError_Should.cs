﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Helper.ErrorLogs;

namespace ReklamiraiSe.Tests.ReklamiraiSeErrorLogs.ErrorLoggerTests
{
	/// <summary>
	/// Trace logs tests
	/// </summary>
    [TestClass]
    public class LogError_Should
    {
        private ErrorLogger _logger;

		/// <summary>
		/// initialize dummy test data
		/// </summary>
		public LogError_Should()
        {
            this._logger = new ErrorLogger();
        }

		/// <summary>
		/// log error data properly
		/// </summary>
        [TestMethod]
        public void CreateErrorLogFileAndWriteLogInIt()
        {
            try
            {
                throw new InvalidCastException("you cannot cast this!");
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex);
            }
        }

		/// <summary>
		/// append error to file
		/// </summary>
        [TestMethod]
        public void AppendErrorLogFileAndWriteLogInIt()
        {
            try
            {
                throw new ArgumentNullException("this cannot be null!");
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex);
            }
        }
    }
}
