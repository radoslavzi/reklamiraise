﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Modules;
using ReklamiraiSe.Tests.Mocks;
using System.Web;
using ReklamiraiSe.Shared;
using System.IO;
using ReklamiraiSe.Shared.Cookies;

namespace ReklamiraiSe.Tests.ReklamiraiSe.Modules.GeoLocationModuleTests
{
    /// <summary>
    /// Summary description for SetRegionCookieIfNotExists_shoudl
    /// </summary>
    [TestClass]
    public class SetRegionCookieIfNotExists_Should
    {
        private LocationCookieModule _module;
        private Moq.Mock<HttpContextBase> _httpContextBase;

		/// <summary>
		/// initialize dummy test data
		/// </summary>
        public SetRegionCookieIfNotExists_Should()
        {
            HttpMock mock = new HttpMock();
            this._httpContextBase = mock.HttpContextBase;
            this._module = new LocationCookieModule(this._httpContextBase.Object);
        }

		/// <summary>
		/// set region cookie when culture module is requested
		/// </summary>
		[TestMethod]
        public void SetCookieWhenEnterFromACountry()
        {
            this._httpContextBase.Object.Request.ServerVariables["REMOTE_ADDR"] = "85.207.21.82";
            this._module.SetRegionCookieIfNotExists();
            
            Assert.IsNotNull(this._httpContextBase.Object.Response.Cookies[Constants.RegionCookieName]);
            Assert.IsNotNull(this._httpContextBase.Object.Response.Cookies[Constants.RegionCookieName].Value);
            Assert.IsFalse(string.IsNullOrWhiteSpace(this._httpContextBase.Object.Response.Cookies[Constants.RegionCookieName].Value));
        }


    }
}
