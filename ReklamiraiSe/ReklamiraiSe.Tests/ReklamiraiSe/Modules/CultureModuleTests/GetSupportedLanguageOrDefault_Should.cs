﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Modules;
using ReklamiraiSe.Shared;
using System.Web;
using ReklamiraiSe.Shared.Cookies;

namespace ReklamiraiSe.Tests.ReklamiraiSe.Modules.CultureModuleTests
{
    /// <summary>
    /// Summary description for GetSupportedLanguageOrDefault_Should
    /// </summary>
    [TestClass]
    public class GetSupportedLanguageOrDefault_Should
    {
        private CultureModule _module;

		/// <summary>
		/// initialize dummy test data
		/// </summary>
		public GetSupportedLanguageOrDefault_Should()
        {
            this._module = new CultureModule(null);
        }

		/// <summary>
		/// get the default application language when cookie is missing
		/// </summary>
        [TestMethod]
        public void GetDefaultLanguageByMissingCookie()
        {
            string code = this._module.GetSupportedCultureOrDefault(null);
            Assert.AreEqual(code, Constants.DefaultCultureCode);
        }

		/// <summary>
		/// get bg language by bg cookie set
		/// </summary>
        [TestMethod]
        public void GetLanguageByBgCookie()
        {
            HttpCookie cookie = Cookie.Create("test", "bg-Bg");
            string code = this._module.GetSupportedCultureOrDefault(cookie);
            Assert.AreEqual(code, Constants.BulgarianCultureCode);
        }

		/// <summary>
		/// test case sensitivity bg cookie
		/// </summary>
        [TestMethod]
        public void GetLanguageByBGCookie()
        {
            HttpCookie cookie = Cookie.Create("test", "bg-BG");
            string code = this._module.GetSupportedCultureOrDefault(cookie);
            Assert.AreEqual(code, Constants.BulgarianCultureCode);
        }

		/// <summary>
		/// test SL cookie set
		/// </summary>
        [TestMethod]
        public void GetDefaultLanguageBySLCookie()
        {
            HttpCookie cookie = Cookie.Create("test", "SL");
            string code = this._module.GetSupportedCultureOrDefault(cookie);
            Assert.AreEqual(code, Constants.DefaultCultureCode);
        }
    }
}
