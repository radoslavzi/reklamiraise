﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Shared;
using ReklamiraiSe.Models;

namespace ReklamiraiSe.Tests.ReklamiraiSeShared.GeneratorTests
{
	/// <summary>
	/// generate new user
	/// </summary>
    [TestClass]
    public class BuildSampleUser_Should
    {
		/// <summary>
		/// generate new user
		/// </summary>
		[TestMethod]
        public void CreateDummyUser()
        {
            User user = Generator.BuildSampleUser();

            Assert.IsNotNull(user);

            #region required fields
            Assert.IsNotNull(user.ConfirmPassword);
            Assert.IsNotNull(user.Name);
            Assert.IsNotNull(user.Password);
            Assert.IsNotNull(user.PhoneNumber);
            Assert.IsNotNull(user.UserName);
            Assert.AreEqual(user.Password, user.ConfirmPassword);
            #endregion
        }
    }
}
