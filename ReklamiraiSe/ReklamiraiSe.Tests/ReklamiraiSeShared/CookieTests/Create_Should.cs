﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web;
using ReklamiraiSe.Shared.Cookies;

namespace ReklamiraiSe.Tests.ReklamiraiSeShared.CookieTests
{
	/// <summary>
	/// Test create operation
	/// </summary>
    [TestClass]
    public class Create_Should
    {
        private string _cookieName;
        private string _cookieValue;

		/// <summary>
		/// initialize dummy test data
		/// </summary>
        public Create_Should()
        {
            this._cookieName = "testName";
            this._cookieValue = "testValue";
        }

		/// <summary>
		/// create cookie test
		/// </summary>
        [TestMethod]
        public void CreateCookieProperly()
        {
            HttpCookie cookie = Cookie.Create(this._cookieName, this._cookieValue);

            Assert.IsNotNull(cookie);
            Assert.AreEqual(cookie.Value, this._cookieValue);
            Assert.AreEqual(cookie.Name, this._cookieName);
            Assert.AreEqual(cookie.HttpOnly, true);
            Assert.AreEqual(cookie.Shareable, true);
            Assert.AreEqual(cookie.Expires, DateTime.MaxValue.ToUniversalTime());
            Assert.AreEqual(cookie.Secure, false);
        }

		/// <summary>
		/// test nullable cookie
		/// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateNullableNameCookie()
        {
            HttpCookie cookie = Cookie.Create(null, this._cookieValue);

            Assert.IsNull(cookie);
        }

		/// <summary>
		/// throws exception when nullable value to cookie is passed
		/// </summary>
        [Timeout(30)]
        [TestMethod]
        public void CreateNullableValueCookie()
        {
            HttpCookie cookie = Cookie.Create(this._cookieName, null);

            Assert.IsNotNull(cookie);
            Assert.IsNull(cookie.Value);
            Assert.AreEqual(cookie.Name, this._cookieName);
            Assert.AreEqual(cookie.HttpOnly, true);
            Assert.AreEqual(cookie.Shareable, true);
            Assert.AreEqual(cookie.Expires, DateTime.MaxValue.ToUniversalTime());
            Assert.AreEqual(cookie.Secure, false);
        }

		/// <summary>
		/// throws exception when nullable cookie is passed
		/// </summary>
        [Timeout(15)]
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateNullableCookie()
        {
            HttpCookie cookie = Cookie.Create(null, null);

            Assert.IsNull(cookie);
        }
    }
}