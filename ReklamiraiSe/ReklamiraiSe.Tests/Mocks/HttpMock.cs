﻿using System.Collections.Specialized;
using System.Web;
using System.Web.Routing;

namespace ReklamiraiSe.Tests.Mocks
{
	/// <summary>
	/// Mock http context request and response data
	/// </summary>
    public class HttpMock
    {
        private Moq.Mock<HttpContextBase> _httpContextBase;
        private Moq.Mock<HttpRequestBase> _httpRequestBase;
        private Moq.Mock<HttpResponseBase> _httpResponseBase;
        private Moq.Mock<RequestContext> _requestContext;
        private Moq.Mock<RouteData> _routeData;

		/// <summary>
		/// initialize dummy httpcotext data
		/// </summary>
		public HttpMock()
        {
            this._httpRequestBase = new Moq.Mock<HttpRequestBase>();
            this._httpResponseBase = new Moq.Mock<HttpResponseBase>();
            this._httpContextBase = new Moq.Mock<HttpContextBase>();
            this._requestContext = new Moq.Mock<RequestContext>();
            this._routeData = new Moq.Mock<RouteData>();
            this.SetupContext();
        }

        private void SetupContext()
        {
            this._httpRequestBase.Setup(req => req.ServerVariables).Returns(new NameValueCollection());
            this._httpRequestBase.Setup(req => req.QueryString).Returns(new NameValueCollection());
            this._httpRequestBase.Setup(req => req.RequestContext).Returns(this._requestContext.Object);
            this._httpRequestBase.Setup(req => req.RequestContext.RouteData).Returns(this._routeData.Object);
            this._httpRequestBase.Setup(req => req.Cookies).Returns(new HttpCookieCollection());
            this._httpResponseBase.Setup(req => req.Cookies).Returns(new HttpCookieCollection());
            this._httpContextBase.Setup(ctx => ctx.Request).Returns(this._httpRequestBase.Object);
            this._httpContextBase.Setup(ctx => ctx.Response).Returns(this._httpResponseBase.Object);
        }

		/// <summary>
		/// get mocked httpcontext
		/// </summary>
        public Moq.Mock<HttpContextBase> HttpContextBase
        {
            get
            {
                return this._httpContextBase;
            }
        }
    }
}
