﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data;
using ReklamiraiSe.Helper.Permissions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.Permissions.RoleManagementTests
{
    /// <summary>
    /// Summary description for CreateRoleIfNotExists_Should
    /// </summary>
    [TestClass]
    public class CreateRoleIfNotExists_Should
    {
        private RoleManagement _roleManagement;
        private string _roleName;
        private RoleManager<IdentityRole> _roleManager;
        private ReklamiraiSeDbContext _context;

		/// <summary>
		/// Test data creation
		/// </summary>
        public CreateRoleIfNotExists_Should()
        {
            this._roleManagement = new RoleManagement();
            this._roleName = "roleTest";
            this._context = ReklamiraiSeDbContext.Create();
        }

		/// <summary>
		/// create application user role
		/// </summary>
        [TestMethod]
        public void CreateRoleProperly()
        {
            this._roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(this._context));
            string role = this._roleManagement.CreateRoleIfNotExists(this._context, this._roleName);

            Assert.IsNotNull(role);
            Assert.AreEqual(role, this._roleName);
            Assert.IsTrue(this._roleManager.RoleExists(this._roleName));
        }

		/// <summary>
		/// not create existing role
		/// </summary>
		[TestMethod]
        public void CreateExistingRoleProperly()
        {
            this._roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(this._context));
            string role = this._roleManagement.CreateRoleIfNotExists(this._context, this._roleName);

            Assert.IsNotNull(role);
            Assert.AreEqual(role, this._roleName);
            Assert.IsTrue(this._roleManager.RoleExists(this._roleName));
        }

		/// <summary>
		/// throws exception when invalid data is passed
		/// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TryCreatingNonValidRoleByMissingContext()
        {
            string role = this._roleManagement.CreateRoleIfNotExists(null, this._roleName);

            Assert.IsNull(role);
        }

		/// <summary>
		/// non valid data is passed
		/// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TryCreatingNonValidRoleByMissingRoleName()
        {
            string role = this._roleManagement.CreateRoleIfNotExists(this._context, null);
            
            Assert.IsNull(role);
        }
    }
}
