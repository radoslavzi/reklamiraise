﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Migrations;
using ReklamiraiSe.Helper.Permissions;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.Migrations.ConfigurationTests
{
    /// <summary>
    /// AddSpecialAdmin_Should must add special admin to db correctly
    /// </summary>
    [TestClass]
    public class AddSpecialAdmin_Should
    {
		private RoleManagement _roleManagement;
		private Data.ReklamiraiSeDbContext _context;

		/// <summary>
		/// Test Admin cruid operations
		/// </summary>
		public AddSpecialAdmin_Should()
		{
			this._roleManagement = new RoleManagement();
			this._context = Data.ReklamiraiSeDbContext.Create();
		}

		/// <summary>
		/// throw exception when context is missing
		/// </summary>
		[TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowExceptionIfMissingContext()
        {
            Configuration configuration = new Configuration();
            configuration.AddSpecialAdmin(null);
        }

		/// <summary>
		/// add application user roles
		/// </summary>
		[TestMethod]
		public void CreateRegularUserRoleIfNotExists()
		{
			Configuration configuration = new Configuration();
			this._roleManagement.CreateRoleIfNotExists(this._context, Shared.Constants.RegularUsersRole);
        }

		/// <summary>
		/// Add admin user to db
		/// </summary>
		[TestMethod]
		public void AddSpecialAdmin()
		{
			Configuration configuration = new Configuration();
			configuration.AddSpecialAdmin(this._context);
		}
	}
}
