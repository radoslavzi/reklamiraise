﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;
using System.Linq;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.ConfigurationTests.EntityOperationsTests
{
	/// <summary>
	/// get data by id from db
	/// </summary>
	[TestClass]
	public class GetById_Should
	{

		/// <summary>
		/// initialize dummy test data
		/// </summary>
		public GetById_Should()
		{
		}

		/// <summary>
		/// get data when no valid id is passed
		/// </summary>
		[TestMethod]
		//[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void ThrowExceptionWhenOutOfRangeIdIsPassedAsParameter()
		{
		}

		/// <summary>
		/// add and get data from db table
		/// </summary>
		[TestMethod]
		public void AddElementToDbAndGetElementWithoutCallToDB()
		{
		}
	}
}
