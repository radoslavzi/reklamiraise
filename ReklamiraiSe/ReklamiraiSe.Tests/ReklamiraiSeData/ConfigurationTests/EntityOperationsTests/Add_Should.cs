﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.ConfigurationTests.EntityOperationsTests
{
	/// <summary>
	/// Test Add to db operation
	/// </summary>
	[TestClass]
	public class Add_Should
	{
		private Message _message;
		private Product _product;

		/// <summary>
		/// Test Add to db operation
		/// </summary>
		public Add_Should()
		{
			this._message = new Message()
			{
				Body = "test body",
				From = "radoslav@gmail.com",
				Destination = "test@gmail.com",
				Subject = "test subject",
				CreationDate = DateTime.Now
			};

			this._product = new Product()
			{
				Title = "test title",
				Description = "test description hfdsakjfhdsalkjhfdsalkjhflkdsa fjdsahlfdashflkjdsahflkdsahlfkdsa hfdaslkjhflkdsajhflkdsaj",
			};

            this._product.Images.Add(new ImageInfo
            {
                Path = "/images/offers/test.png",
                Alt = "alt",
                Title = "title"
            });
		}

		/// <summary>
		/// Throws exception when attempting to add no data
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ThrowExceptionIfDataIsNull()
		{
            EntitiesRepository.Feedbacks.Add(null);
		}

		/// <summary>
		/// Save data to db
		/// </summary>
		[TestMethod]
		public void SaveDataToDbAndCache()
		{
            EntitiesRepository.Messages.Add(this._message);
			int id = this._message.Id;

            Assert.IsNotNull(EntitiesCacheRepository.MessagesCache);
            Assert.IsTrue(!EntitiesCacheRepository.MessagesCache.IsEmpty());
            Assert.IsTrue(EntitiesCacheRepository.MessagesCache.Get(id).IsActive);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Body, this._message.Body);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Destination, this._message.Destination);
		}

		/// <summary>
		/// save category in bg language
		/// </summary>
		[TestMethod]
		public void SaveOfferBGCodeToDbAndCache()
		{
			//for (int i = 0; i < 1000; i++)
			{
				this._product = new Product
				{
					Title = "test title",
					Description = "test description hfdsakjfhdsalkjhfdsalkjhflkdsa fjdsahlfdashflkjdsahflkdsahlfkdsa hfdaslkjhflkdsajhflkdsaj",
				};

                this._product.Images.Add(new ImageInfo
                {
                    Path = "/images/offers/test.png",
                    Alt = "alt",
                    Title = "title"
                });



                EntitiesRepository.Products.Add(this._product);
			}
		}

		/// <summary>
		/// save category in en language
		/// </summary>
		[TestMethod]
		public void SaveOfferENCodeToDbAndCache()
		{
            EntitiesRepository.Products.Add(this._product);
			int id = this._product.Id;
			Assert.IsNotNull(EntitiesCacheRepository.ProductsCache);
            Assert.IsTrue(!EntitiesCacheRepository.ProductsCache.IsEmpty());
		}
	}
}
