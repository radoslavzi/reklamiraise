﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.ConfigurationTests.EntityOperationsTests
{
	/// <summary>
	/// Update db operation tests
	/// </summary>
	[TestClass]
	public class UpdateData_Should
	{
		private Message _message;

		/// <summary>
		/// initialize dummy update data
		/// </summary>
		public UpdateData_Should()
		{
			this._message = new Message()
			{
				Body = "test body",
				From = "radoslav@gmail.com",
				Destination = "test@gmail.com",
				Subject = "test subject",
				CreationDate = DateTime.Now,
			};
		}

		/// <summary>
		/// update properly data
		/// </summary>
		[TestMethod]
		public void UpdateDataProperly()
		{
            EntitiesRepository.Messages.Add(this._message);
			this._message.Body = "new body test";
			this._message.Destination = "newdest@gmail.com";
			this._message.From = "newfrom@gmail.com";
			this._message.Subject = "new subject";

            EntitiesRepository.Messages.Update(this._message.Id, this._message);
			int id = this._message.Id;

			Assert.IsTrue(this._message.IsActive);
			Assert.IsNotNull(EntitiesCacheRepository.MessagesCache);
            Assert.IsTrue(!EntitiesCacheRepository.MessagesCache.IsEmpty());
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Destination, this._message.Destination);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Body, this._message.Body);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Subject, this._message.Subject);
            Message message = EntitiesRepository.Messages.GetById(this._message.Id);

			Assert.AreEqual(id, message.Id);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Destination, message.Destination);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Body, message.Body);
            Assert.AreEqual(EntitiesCacheRepository.MessagesCache.Get(id).Subject, message.Subject);
        }
	}
}
