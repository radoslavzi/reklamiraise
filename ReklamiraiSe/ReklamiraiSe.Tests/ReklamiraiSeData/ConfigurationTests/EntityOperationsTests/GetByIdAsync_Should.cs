﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.ConfigurationTests.EntityOperationsTests
{
	/// <summary>
	/// get data from db and cache test
	/// </summary>
	[TestClass]
	public class GetByIdAsync_Should
	{
		/// <summary>
		/// initialize dummy test data
		/// </summary>
		public GetByIdAsync_Should()
		{
		}

		/// <summary>
		/// get added element from db table
		/// </summary>
		[TestMethod]
		public void AddElementToDbAsyncAndGetElementWithoutCallToDB()
		{
		}
	}
}
