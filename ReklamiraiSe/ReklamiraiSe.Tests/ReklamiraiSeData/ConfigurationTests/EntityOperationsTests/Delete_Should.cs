﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;

namespace ReklamiraiSe.Tests.ReklamiraiSeData.ConfigurationTests.EntityOperationsTests
{
	/// <summary>
	/// test delete operations
	/// </summary>
	[TestClass]
	public class Delete_Should
	{
		private Message _message;

		/// <summary>
		/// initialize dummy test data
		/// </summary>
		public Delete_Should()
		{
			this._message = new Message()
			{
				Body = "test body",
				Destination = "dest@gmail.com",
				From = "abv@abv.bg",
				Subject = "test subject",
				CreationDate = DateTime.Now
			};
        }

		/// <summary>
		/// delete message table data + cache
		/// </summary>
		[TestMethod]
		public void DeleteMessageFromDbAndCache()
		{
            EntitiesRepository.Messages.Add(this._message);
			int id = this._message.Id;

			Assert.IsNotNull(EntitiesCacheRepository.MessagesCache);
            Assert.IsTrue(!EntitiesCacheRepository.MessagesCache.IsEmpty());
            Assert.IsTrue(EntitiesCacheRepository.MessagesCache.Get(id).IsActive);
            EntitiesRepository.Messages.Delete(id);
			Assert.IsFalse(EntitiesCacheRepository.MessagesCache.Get(id).IsActive);
        }

		/// <summary>
		/// delete category data and cache
		/// </summary>
		[TestMethod]
		public void DeleteCategoryByIdFromDbAndCache()
		{
		}
	}
}
