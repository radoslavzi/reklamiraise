﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ReklamiraiSe.Models
{
    public class ProductViewModel
    {
        [MinLength(5, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_title_validation_error")]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_title_required_error")]
        [Display(Name = "offers_title", ResourceType = typeof(Resources.Resource))]
        public string Title { get; set; }

        [Display(Name = "offers_description", ResourceType = typeof(Resources.Resource))]
        [MinLength(100, ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_description_validation_error")]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_description_required_error")]
        public string Description { get; set; }

        [Display(Name = "offers_tags", ResourceType = typeof(Resources.Resource))]
        public string Tags { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_price_validation")]
        [Display(Name = "offers_price", ResourceType = typeof(Resources.Resource))]
        public decimal PricePerHour { get; set; }

        [Display(Name = "offers_images", ResourceType = typeof(Resources.Resource))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resource), ErrorMessageResourceName = "offers_images_required_error")]
        public IEnumerable<HttpPostedFileBase> Images { get; set; }
    }
}