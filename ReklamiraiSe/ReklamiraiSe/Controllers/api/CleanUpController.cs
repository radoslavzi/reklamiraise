﻿using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Data.Configuration.Interface;
using ReklamiraiSe.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;

namespace ReklamiraiSe.Controllers.api
{
    // TODO: shrink access to a particular user
    public class CleanUpController : ApiController
    {
        private IAsyncEntityOperations<Product, int> _productsManager;
        private IAsyncEntityOperations<ImageInfo, int> _imagesManager;
        private IAsyncEntityOperations<Comment, int> _commentsManager;

        public CleanUpController()
        {
            this._productsManager = EntitiesRepository.Products;
            this._imagesManager = EntitiesRepository.Images;
            this._commentsManager = EntitiesRepository.Comments;
        }

        // DELETE: api/CleanUp/5
        [HttpDelete, Authorize(Roles = Shared.Constants.SpecialAdminRole)]
        public void DeleteOfferByIdAsync(int id)
        {
            this._productsManager.DeleteAsync(id);
        }

        // DELETE: api/CleanUp/5
        [HttpDelete, Authorize(Roles = Shared.Constants.SpecialAdminRole)]
        public void DeleteOffersAsync(int id)
        {
            IQueryable<Product> offers = this._productsManager.All();
            foreach (Product offer in offers)
            {
                foreach (var image in offer.Images)
                {
                    File.Delete(image.Path);
                    this._imagesManager.DeleteAsync(image.Id);
                }

                foreach (var comment in offer.Comments)
                {
                    this._commentsManager.DeleteAsync(comment.Id);
                }

                this._productsManager.DeleteAsync(offer.Id);
            }
        }
    }
}
