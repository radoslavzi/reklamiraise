﻿using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Models;
using System;
using ReklamiraiSe.Attributes;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Drawing;
using ReklamiraiSe.Helper.Image;
using System.Web.UI;

namespace ReklamiraiSe.Controllers
{
    [LevGrowAuthorization]
    public class OffersController : BaseController
    {
        public ActionResult Get(int id)
        {
            ViewBag.Title = Resources.Resource.offer_get_page_title;
            ViewBag.Description = Resources.Resource.offer_get_page_description;

            var offer = EntitiesCacheRepository.OffersCache.Get(id);

            if (offer != null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(offer);
        }

        [LevGrowAuthorization]
        public ActionResult Edit(int id)
        {
            var product = EntitiesCacheRepository.OffersCache.Get(id);

            if (product!=null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(product);
        }

        [LevGrowAuthorization, HttpPost]
        public ActionResult Edit(Product model)
        {
            if (ModelState.IsValid)
            {
                this.UpdateImages(model);
                EntitiesRepository.Offers.Update(model.Id, model);
            }

            return RedirectToActionPermanent("Index", "Home");
        }

        private void UpdateImages(Product model)
        {

        }

        [LevGrowAuthorization]
        public ActionResult Create()
        {
            return View();
        }

        [LevGrowAuthorization, HttpPost, ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None", Location = OutputCacheLocation.None)]
        public ActionResult Create([Bind()]OfferViewModel model)
        {
            if (ModelState.IsValid)
            {
                Product offer = this.CreateOfferByModel(model);
                this.ProcessImages(offer, Request.Files, model.Title);
                EntitiesRepository.Offers.Add(offer);
                return RedirectToAction("Get", new { id = offer.Id });
            }

            return View(model);
        }

        private Product CreateOfferByModel(OfferViewModel model)
        {
            return new Product() {
                Description = model.Description,
                Price = model.PricePerHour,
                Tags = model.Tags,
                Title = model.Title,
            };
        }

        private void ProcessImages(Product model, HttpFileCollectionBase files, string title)
        {
            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    HttpPostedFileBase file = files.Get(i);
                    string date = DateTime.UtcNow.ToString("dd/MM/yyyy");
                    string uploadFolder = VirtualPathUtility.ToAbsolute(string.Format("{0}/{1}/{2}/", "~/Images/uploads", User.Identity.Name, date));

                    string absolutePath = Path.Combine(Server.MapPath("~/Images/uploads"), string.Format("{0}/{1}/", User.Identity.Name, date));
                    if (!Directory.Exists(absolutePath))
                    {
                        Directory.CreateDirectory(absolutePath);
                    }
                    string fileExt = Path.GetExtension(file.FileName);
                    string smallFileName = file.FileName + "_small" + fileExt;
                    string bigFileName = file.FileName + "_big" + fileExt;

                    Bitmap map = new Bitmap(Image.FromStream(file.InputStream, true));
                    ImageHelper.Save(map, 100, 100, 255, Path.Combine(absolutePath, smallFileName));
                    ImageHelper.Save(map, map.Width, map.Height, 255, Path.Combine(absolutePath, bigFileName));

                    ImageInfo info = new ImageInfo
                    {
                        Title = title,
                        Path = VirtualPathUtility.Combine(uploadFolder, smallFileName)
                    };

                    model.Images.Add(info);
                }
                catch (Exception ex)
                {
                    this.Logger.LogError(ex);
                }
            }
        }

        //[Authorize, HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //[Authorize, HttpPost]
        //public void Archive(int id)
        //{
        //    var offer = EntitiesCacheRepository.OffersCache.Get(id);

        //    if (!offer.IsActive)
        //    {
        //        return;
        //    }

        //    offer.ExpirationDate = DateTime.UtcNow;
        //    EntitiesRepository.Offers.Update(id, offer);
        //}

        //[Authorize, HttpPost]
        //public ActionResult Delete(int id)
        //{
        //    try
        //    {
        //        var offer = EntitiesCacheRepository.OffersCache.Get(id);

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
