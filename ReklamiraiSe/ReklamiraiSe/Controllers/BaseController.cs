﻿using ReklamiraiSe.Helper.ErrorLogs;
using System.Web.Mvc;

namespace ReklamiraiSe.Controllers
{
    public class BaseController : Controller
    {
        protected IErrorLogger Logger;

        public BaseController()
        {
            this.Logger = new ErrorLogger();
        }
    }
}