﻿using System.Web.Mvc;

namespace ReklamiraiSe.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Description = Resources.Resource.home_meta_description;
            ViewBag.Title = Resources.Resource.home_page_title;

            return View();
        }

        //[ChildActionOnly]
        //public PartialViewResult LastOffersList(ushort page = 0)
        //{
        //    //TODO: add in depends on country
        //    IQueryable<Product> model = null;
        //    if (!EntitiesCacheLoader.OffersThread.IsCompleted)
        //    {
        //        EntitiesCacheLoader.OffersThread.ContinueWith((x) => LastOffersList(page));
        //    }

        //    model = EntitiesCacheRepository.OffersCache.All()
        //                    .Skip(page * _pageSize).Take(_pageSize);

        //    return PartialView("_PartialOffersList", model);
        //}

        //public ActionResult ChangeCulture(string cultureCode, string returnUrl)
        //{
        //    Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(cultureCode);
        //    HttpCookie cookie = Cookie.GetByName(Constants.RegionCookieName, HttpContext);
        //    cookie.Value = cultureCode;
        //    HttpContext.Response.Cookies.Add(cookie);

        //    return Redirect(returnUrl);
        //}
    }
}