﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace ReklamiraiSe.App_Start.Binders
{
    public class DecimalModelBinder : IModelBinder
    {
        /*
             http://haacked.com/archive/2011/03/19/fixing-binding-to-decimals.aspx/
        */
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            ModelState modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                if (!string.IsNullOrEmpty(valueResult.AttemptedValue))
                {
                    actualValue = Convert.ToDecimal(valueResult.AttemptedValue,
                  CultureInfo.InvariantCulture);
                }
                else
                {
                    modelState.Errors.Add(new MissingFieldException(Resources.Resource.offers_price_validation));
                }
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }
}