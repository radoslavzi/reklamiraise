﻿using ReklamiraiSe.App_Start.Routing;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReklamiraiSe
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var localizingRoute = 
                new AutoLocalizingRoute("{culture}/{controller}/{action}/{id}",
                     new { id = UrlParameter.Optional }, new { culture = "^[a-z]+-[A-Z]+$" });
            RouteTable.Routes.Add("LocalizingRoute", localizingRoute);

            routes.MapRoute("Default",
                        url: "{controller}/{action}/{id}",
                        defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
