﻿using System.Web.Optimization;

namespace ReklamiraiSe
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                            "~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                           "~/Scripts/microajax.minified.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/scripts/default").Include(
                      "~/Scripts/angular.min.js",
                      "~/Scripts/module.js"));
        }
    }
}
