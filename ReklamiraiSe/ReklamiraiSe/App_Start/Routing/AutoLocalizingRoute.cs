﻿using ReklamiraiSe.Shared;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;

namespace ReklamiraiSe.App_Start.Routing
{
    public class AutoLocalizingRoute : Route
    {
        public AutoLocalizingRoute(string url, object defaults, object constraints)
        : base(url, new RouteValueDictionary(defaults), new RouteValueDictionary(constraints), new MvcRouteHandler())
        { }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            // only set the culture if it's not present in the values dictionary yet
            // this check ensures that we can link to a specific language when we need to (fe: when picking your language)
            if (!values.ContainsKey(Constants.CultureRouteKey))
            {
                values[Constants.CultureRouteKey] = Thread.CurrentThread.CurrentCulture;
            }

            return base.GetVirtualPath(requestContext, values);
        }
    }
}