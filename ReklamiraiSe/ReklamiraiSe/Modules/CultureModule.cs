﻿using ReklamiraiSe.Helper.Localization;
using ReklamiraiSe.Shared;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Routing;

namespace ReklamiraiSe.Modules
{
    public class CultureModule : IHttpModule
    {
        private HttpContextBase _httpContextBase;

        public CultureModule()
        {
            //register all supported languages
            CulturesLoader.RegisterLanguages();
        }

        public CultureModule(HttpContextBase httpContextBase)
            : this()
        {
            this._httpContextBase = httpContextBase;
        }

        public void Dispose() { }

        public void Init(HttpApplication context)
        {
            if (context == null)
            {
                throw new EntryPointNotFoundException("CultureModule: context not found exception!");
            }

            context.BeginRequest += new EventHandler(OnBeginRequest);
        }

        public void OnBeginRequest(Object source, EventArgs e)
        {
            this._httpContextBase = new HttpContextWrapper(HttpContext.Current);
            HttpCookie rgnCookie = this._httpContextBase.Request.Cookies[Constants.RegionCookieName];
            string culture = this.GetSupportedCultureOrDefault(rgnCookie);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }

        public string GetSupportedCultureOrDefault(HttpCookie rgnCookie)
        {
            if (rgnCookie != null && CultureCache.LanguageProvider.ContainsKey(rgnCookie.Value))
            {
                CultureCache.CurrentLanguageCode = CultureCache.LanguageProvider[rgnCookie.Value].Code;
                return CultureCache.LanguageProvider[rgnCookie.Value].Code;
            }
            else
            {
                CultureCache.CurrentLanguageCode = Constants.DefaultCultureCode;
                return Constants.DefaultCultureCode;
            }
        }
    }
}
