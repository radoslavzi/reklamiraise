﻿using System;
using System.Web;
using ReklamiraiSe.Shared;
using System.Net;
using System.IO;
using NGeoIP;
using NGeoIP.Client;
using System.Globalization;

namespace ReklamiraiSe.Modules
{
    public class LocationCookieModule : IHttpModule
    {
        private HttpContextBase _httpContextBase;

        public LocationCookieModule() { }

        public LocationCookieModule(HttpContextBase httpContextBase)
            : base()
        {
            this._httpContextBase = httpContextBase;
        }

        public void Dispose() { }

        public void Init(HttpApplication context)
        {
            if (context == null)
            {
                throw new EntryPointNotFoundException("GeoLocationModule: context not found exception!");
            }

            context.BeginRequest += new EventHandler(OnBeginRequest);
        }

		private void OnBeginRequest(object sender, EventArgs e)
		{
			this._httpContextBase = new HttpContextWrapper(HttpContext.Current);
			this.SetRegionCookieIfNotExists();
        }

		public void SetRegionCookieIfNotExists()
        {
            HttpCookie rgnCookie = this._httpContextBase.Request.Cookies[Constants.RegionCookieName];
            if (rgnCookie == null)
            {
                var culture = CultureInfo.CurrentCulture;
                HttpCookie cookie = Shared.Cookies.Cookie.Create(Constants.RegionCookieName, culture.Name);
                this._httpContextBase.Response.Cookies.Set(cookie);
            }
        }
    }
}