﻿(function () {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        roundLengths: true,
        autoplay: 6000,
        watchSlidesProgress: true,
        watchSlidesVisibility: true,
        loop: true
    });
})();