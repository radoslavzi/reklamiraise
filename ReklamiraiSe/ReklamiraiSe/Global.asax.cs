﻿using System;
using System.Data.Entity;
using ReklamiraiSe.Data.Migrations;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ReklamiraiSe.Data;
using System.Web.Http;
using ReklamiraiSe.Data.Configuration;
using ReklamiraiSe.Helper.ErrorLogs;
using ReklamiraiSe.Data.Configuration.Interface;
using ReklamiraiSe.App_Start.Binders;

namespace ReklamiraiSe
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ErrorLogger _errorLogger;
        private IEntitiesCacheLoader _cacheLoader;

        public MvcApplication()
        {
            this._cacheLoader = new EntitiesCacheLoader();
        }
        
        protected void Application_Start()
        {
            this._cacheLoader.LoadDataToCacheAsync();

            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ReklamiraiSeDbContext>());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ReklamiraiSeDbContext, Configuration>());
        }

        protected void Application_End()
        {
            GC.Collect();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            if (exc != null)
            {
                this._errorLogger = new ErrorLogger();
                this._errorLogger.LogError(exc);
            }

            Server.ClearError();
        }
    }
}
