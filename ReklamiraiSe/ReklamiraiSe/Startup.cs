﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReklamiraiSe.Startup))]
namespace ReklamiraiSe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
