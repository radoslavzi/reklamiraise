﻿using ReklamiraiSe.Data;
using ReklamiraiSe.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
 
namespace ReklamiraiSeInitializer
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ReklamiraiSeDbContext, Configuration>()); 

        }
    }
}
