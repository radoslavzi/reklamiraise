﻿using System;
using NUnit.Framework;
using ReklamiraiSeAPI.MailAPI;
using System.Net.Mail;

namespace ReklamiraiSeAPI.Tests
{
    [TestFixture]
    public class MailApiTest
    {
        private IMailApi _mailApi;
        private string _mailAddress;
        public MailApiTest()
        {
            _mailAddress = "radoslav.zlatev@gmail.com";
            this._mailApi = new MailApi("subject", "body");
        }

        [Test]
        public void CheckRightMailData()
        {
            MailMessage message = this._mailApi.ConstructMessage(_mailAddress);
            
            Assert.IsNotNull(message);
            Assert.AreEqual(message.Body, "body");
            Assert.AreEqual(message.Subject, "subject");
        }

        [Test]
        public void SendMessage()
        {
            MailMessage message = this._mailApi.ConstructMessage(_mailAddress);
            this._mailApi.SendMail(message);
        }
    }
}
