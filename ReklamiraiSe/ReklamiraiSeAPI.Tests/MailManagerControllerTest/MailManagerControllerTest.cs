﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using NUnit.Framework;
using ReklamiraiSeAPI.Controllers;

namespace ReklamiraiSeAPI.Tests.MailManagerControllerTest
{
    [TestFixture]
    public class MailManagerControllerTest : MockController
    {
        private MailManagerController _controller;

        public MailManagerControllerTest()
        {
            this._controller = new MailManagerController();
            this._controller.ControllerContext = new HttpControllerContext(config, routeData, request);
            this._controller.Request = request;
            this._controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
        }

        [Test]
        public void SendMail_Properly()
        {
            HttpResponseMessage response = _controller.SendMail("radoslav.zlatev@gmail.com","subject", "body");
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [Test]
        public void SendMail_WithNonValidEmail()
        {
            HttpResponseMessage response = _controller.SendMail("radoslav@zlatev@gmail.com", "subject", "body");
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);
        }
    }
}
