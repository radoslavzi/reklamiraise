﻿using NUnit.Framework;
using System;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace ReklamiraiSeAPI.Tests
{
    [TestFixture]
    public class MockController
    {

        protected HttpConfiguration config;
        protected HttpRequestMessage request ;
        protected IHttpRoute route ;
        protected HttpRouteData routeData;

        public MockController()
        {
            this.config = new HttpConfiguration();
            this.request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/products");
            this.route = config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}");
            this.routeData = new HttpRouteData(route, new HttpRouteValueDictionary { { "controller", "products" } });
        }
    }
}
